package co.unicauca.restaurant.orientalApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import co.unicauca.restaurant.accces.IProductRepository;
import co.unicauca.restaurant.accces.ProductRepositoryImplArraysItaliano;
import co.unicauca.restaurant.domain.Product;
import co.unicauca.restaurant.domain.Size;
import co.unicauca.restaurant.services.DishBuilder;

public class ItalianoDishBuilder extends DishBuilder{
    IProductRepository myRepository;
    ItalianoDish myItalianoDish;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    List<Product> allProducts;
    List<Product>  bases;
    List<Product>  options;
    List<Product> postre;

    private Product read(List<Product> myProducts, String foodPart) {
        String input = "";
        try {
            int i =1;
            System.out.println("Seleccione una "+foodPart);
           for (Product each: myProducts){
               System.out.println(""+i+". "+ each.getName() + ":"+each.getPrice());
               i++;
           }   
          System.out.println("Ingrese el código de la "+ foodPart);
          input = br.readLine();
        } catch (IOException e) {
        }
        return myProducts.get(Integer.parseInt(input)-1);
      }

      private String read(String message) {
        String input = "";
        try {
          System.out.println(message);
          input = br.readLine();
        } catch (IOException e) {
        }
        return input;
      }
    
      @Override
      public DishBuilder init() { 
        myRepository = new ProductRepositoryImplArraysItaliano();
        myDish = new ItalianoDish(0.0);
        myItalianoDish = (ItalianoDish) myDish;
        //Obtenemos todos los productos
        allProducts = myRepository.findAll();
        // Obtenemos las bases y las opciones
        bases = new ArrayList<>();
        options = new ArrayList<>();
        postre = new ArrayList<>();
        for(Product each: allProducts){
            if(each.getId()<4)
                bases.add(each);
            else {
              if(each.getId()<7)
                 options.add(each);
              else
                  postre.add(each);
            }
        }
        return this ;
      }
      
    @Override
    public DishBuilder setCore() {
        myItalianoDish.setBase(read(bases, "Entrada"));
        return this;
    }

    @Override
    public boolean addPrincipal() {
        myItalianoDish.addOption(read(options, "Plato principal"));
        return ("S".equals(read("Presione S para más opciones")));
     
    }

    @Override
    public boolean addPart() {
        myItalianoDish.addOption(read(postre, "Postre"));
        return ("S".equals(read("Presione S para más opciones")));
     
    }

    @Override
    public DishBuilder setSize() {
      String tamano = read("Presione la letra correspondiente para el tamaño Personal(P), Doble (D), Familiar (F)");
      if(tamano.equals("F")) myItalianoDish.setSize(Size.FAMILY);
      if(tamano.equals("D")) myItalianoDish.setSize(Size.DOUBLE);
      if(tamano.equals("P")) myItalianoDish.setSize(Size.PERSONAL);
      return this;
    }

}
