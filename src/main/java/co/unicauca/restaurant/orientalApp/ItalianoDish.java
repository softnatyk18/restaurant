package co.unicauca.restaurant.orientalApp;

import java.util.List;
import java.util.Vector;

import co.unicauca.restaurant.domain.Dish;
import co.unicauca.restaurant.domain.Product;
import co.unicauca.restaurant.domain.Size;

public class ItalianoDish extends Dish {
    private Product base;
    private List<Product> myOptions;
    private Size size;

    ItalianoDish(double p) {
        this.price = p;
        setMyOptions(new Vector<>());
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }
    
    void addOption(Product option) {
        getMyOptions().add(option);
    }

    public List<Product> getMyOptions() {
        return myOptions;
    }

    public void setMyOptions(List<Product> myOptions) {
        this.myOptions = myOptions;
    }

    public Product getBase() {
        return base;
    }

    public void setBase(Product base) {
        this.base = base;
    }

    @Override
    public double getPrice(){
        price = base.getPrice();
        for(Product each: myOptions){
            price = price + each.getPrice();
        }
        if(size==Size.DOUBLE) price = 1.8*price;
        if(size==Size.FAMILY) price = 3*price;
        return price;
    }

}
