/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.unicauca.restaurant.services;

import co.unicauca.restaurant.domain.Dish;

/**
 *
 * @author ahurtado
 */
public abstract class DishBuilder {
    
    protected Dish myDish;
    
    Dish getDish() {
        return myDish;
    }

    public  DishBuilder init(){return this;};

    public DishBuilder setCore(){return this;};

    public boolean addPart(){return false;};

    public DishBuilder setSize(){return this;};

    public boolean addPrincipal(){return false;};
    
}
