package co.unicauca.restaurant.services;

import co.unicauca.restaurant.services.DishBuilder;
import co.unicauca.restaurant.domain.Dish;


/**
 *
 * @author Julio Hurtado, Libardo Pantoja
 * 
 */ 

public class DishDirector {
  
  private DishBuilder builder;

  public DishDirector(DishBuilder builder) {
    this.builder = builder;
  }

  public Dish getDish() {
    return builder.getDish();
  }

  public void create() {
    boolean masPartes = true;
    boolean principal = true;
    builder.init();
    builder.setCore();
   
    while(principal){
      principal =builder.addPrincipal();
    }
    while(masPartes){
      masPartes = builder.addPart();
    }
    
    builder.setSize();
}
}
