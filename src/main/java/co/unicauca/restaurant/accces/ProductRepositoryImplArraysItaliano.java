/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.unicauca.restaurant.accces;
import co.unicauca.restaurant.domain.Product;
import java.util.ArrayList;
import java.util.List;

/**
* Implementación por defecto. El framewok contenedor de CDI (Contexts and
* Dependency Injection) carga la implementación por defecto.
*
* @author Libardo, Julio
*/

public class ProductRepositoryImplArraysItaliano implements IProductRepository {

/**
* Por simplicidad los datos se cargan en un array.
*/
    public static List<Product> products;
    
    public ProductRepositoryImplArraysItaliano(){
        products = new ArrayList<>();
        initialize();
    }
    /**
     * entrada
     * plato principal
     * postre
     */
    private void initialize() {
        products.add(new Product(1, "Lasaña de carne Picada", 5000d));
        products.add(new Product(2, "Pasta boloñesa", 5500d));
        products.add(new Product(3, "Rabioles fritos", 5000d));
         products.add(new Product(4, "Espaguetis carbonara", 5800d));
        products.add(new Product(5, "Bolas de arroz Italiana", 6900d));
        products.add(new Product(6, "Macarrones Carbonara", 6200d));
        products.add(new Product(7, "Tiramisu", 6200d));
        products.add(new Product(8, "Copa Gelato", 6200d));
        products.add(new Product(9, "Gelato de fresa natural", 6200d));
        
    }
    
    @Override
    public List<Product> findAll() {
        return products;
    }

    @Override
    public Product findById(Integer id) {
        for(Product prod:products){
            if (prod.getId() == id) {
                return prod;
            }
        }
        return null;
    }

    @Override
    public boolean create(Product newProduct) {
        Product prod = this.findById(newProduct.getId());
        if (prod != null) {
                //Ya existe
                return false;
        }
        products.add(newProduct);
        return true;
    }

    @Override
    public boolean update(Product newProduct) {
        Product prod = this.findById(newProduct.getId());
        if (prod != null) {
            prod.setName(newProduct.getName());
            prod.setPrice(newProduct.getPrice());
        return true;
        }
    return false;
    }

    @Override
    public boolean delete(Integer id) {
        for (Product prod : products) {
                if (prod.getId() == id) {
                    products.remove(prod);
                    return true;
                }
        }
        return false;
    }
}
